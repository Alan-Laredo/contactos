<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190528162956 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE todo ADD recordatorio DATETIME NOT NULL, ADD done TINYINT(1) NOT NULL, DROP year_start, DROP month_start, DROP day_start, DROP year_end, DROP month_end, DROP day_end, CHANGE description descripcion VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE todo ADD year_start INT NOT NULL, ADD month_start INT NOT NULL, ADD day_start INT NOT NULL, ADD year_end INT DEFAULT NULL, ADD month_end INT DEFAULT NULL, ADD day_end INT DEFAULT NULL, DROP recordatorio, DROP done, CHANGE descripcion description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
