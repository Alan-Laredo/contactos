<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contact", name="contact.")
 */
class ContactController extends AbstractController
{
    // db connection
    private static $db;
    private static $initialized = false;

    private static function initialize()
    {
        if (self::$initialized)
            return;

        self::$db = DataController::connect();
    }

    public static function getContacts($id = null)
    {
        self::initialize();
        $query = 'SELECT
                *,
                CONCAT(c.nombres, " ", c.apellidos) AS "name"
            FROM
                contact c
            WHERE
                c.tag = "paciente"';
        if ($id) {
            $query .= 'AND
                c.id = :id';
        }
        $stmt = self::$db->prepare($query);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $contact = $stmt->fetch();
        $stmt->closeCursor();
        return $contact;
    }

    public static function getPatients()
    {
        self::initialize();
        return self::$db->fetchAll(
            'SELECT
                *,
                CONCAT(c.nombres, " ", c.apellidos) AS "name"
            FROM
                contact c
            WHERE
                c.tag = "paciente"'
        );
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(ContactRepository $contactRepository): Response
    {
        return $this->render('contact/index.html.twig', [
            'title' => 'Contactos',
            'contacts' => $contactRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        dump($form);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('contact.index');
        }

        return $this->render('contact/new.html.twig', [
            'title' => 'Nuevo Contacto',
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Contact $contact): Response
    {
        return $this->render('contact/show.html.twig', [
            'title' => 'Contacto #' . $contact->getId(),
            'contact' => $contact,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contact $contact): Response
    {
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact.index');
        }

        return $this->render('contact/edit.html.twig', [
            'title' => 'Editar Contacto #' . $contact->getId(),
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contact $contact): Response
    {
        if ($this->isCsrfTokenValid('delete' . $contact->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contact);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contact.index');
    }
}
