<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * @Route("/consult", name="consult.")
 */
class ConsultController extends AbstractController
{
    // db connection
    private static $db;
    private static $initialized = false;

    public function __construct()
    {
        self::initialize();
    }

    private static function initialize()
    {
        if (self::$initialized)
            return;

        self::$db = DataController::connect();
    }

    // read and parse consults
    public static function getConsults($id = null)
    {
        self::initialize();
        $query = 'SELECT
                c.id,
                c.recordatorio,
                c.duracion,
                c.paciente,
                CONCAT(p.nombres, " ", p.apellidos) AS   "nombrePaciente",
                c.descripcion
            FROM
                consult c 
                LEFT JOIN contact p ON
                    c.paciente = p.id
            WHERE c.active = 1';
        if (isset($id)) {
            $query .= ' AND c.id = :id ';
        }
        $stmt = self::$db->prepare($query);
        if (isset($id)) {
            $stmt->bindValue(':id', $id);
        }
        $stmt->execute();
        $consults = $stmt->fetchAll();
        $stmt->closeCursor();
        dump(['consults' => $consults]);
        return $consults;
    }

    private function handleForm(Request $request, $data = null)
    {

        $patients = ContactController::getPatients();
        dump(['patientes' => $patients]);
        $choices = [];
        foreach ($patients as $patient) {
            $choices[$patient['name']] = $patient['id'];
        }
        dump(['chioces' => $choices]);
        $form = $this->createFormBuilder($data)
            ->add('descripcion', TextareaType::class)
            ->add('paciente', ChoiceType::class, [
                'placeholder' => 'Paciente',
                'choices' => $choices,
            ])
            ->add('recordatorio', DateTimeType::class, [
                'placeholder' => [
                    'year' => 'Año', 'month' => 'Mes', 'day' => 'Día', 'hour' => 'Hora', 'minute' => 'Minuto',
                ],
            ])
            ->add('duracion', TimeType::class, [
                'placeholder' => [
                    'hour' => 'Horas', 'minute' => 'Minutos',
                ],
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitedData = $form->getData();
            dump(['submitedData' => $submitedData, 'data' => $data]);
            try {
                $query = '';
                if ($data) {
                    $query = "UPDATE consult
                        SET descripcion = :descripcion, paciente = :paciente, recordatorio = :recordatorio, duracion = :duracion 
                        WHERE id = :id";
                } else {
                    $query = "INSERT 
                        INTO consult (descripcion, paciente, recordatorio, duracion) 
                        VALUES (:descripcion, :paciente, :recordatorio, :duracion)";
                }
                $stmt = self::$db->prepare($query);
                if ($data) {
                    $stmt->bindValue(':id', $data['id']);
                }
                $stmt->bindValue(':descripcion', $submitedData['descripcion']);
                $stmt->bindValue(':paciente', $submitedData['paciente']);
                $stmt->bindValue(':recordatorio', $submitedData['recordatorio']->format('Y-m-d H:i:s'));
                $stmt->bindValue(':duracion', $submitedData['duracion']->format('Y-m-d H:i:s'));
                $stmt->execute();
                $stmt->closeCursor();
            } catch (\Throwable $th) {
                throw $th;
                error_log($th);
            }
        }

        return $form;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $co = array();
        foreach (self::getConsults() as $consult) {
            $duracionArray = explode(':', $consult['duracion']);

            $recordatorio = new \DateTime($consult['recordatorio']);
            $consult['recordatorio'] = $recordatorio->format('c');

            $duracion = new \DateTime($consult['duracion']);
            $consult['duracion'] = $duracion->format('c');

            $duracion = new \DateInterval('PT' . $duracionArray[0] . 'H' . $duracionArray[1] . 'M' . $duracionArray[2] . 'S');
            $consult['fin'] = $recordatorio->add($duracion)->format('c');
            $co[] = $consult;
        }

        $consults = $co;
        return $this->render('consult/index.html.twig', [
            'title' => 'Consultorio',
            'consults' => $consults,
        ]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request): Response
    {
        $form = $this->handleForm($request);
        if ($form->isSubmitted()) {
            return $this->redirectToRoute('consult.index');
        }

        return $this->render('consult/form.html.twig', [
            'title' => 'Nueva cita',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, $id): Response
    {
        try {
            error_log('delete');
            $query = "UPDATE consult
                SET active = 0
                WHERE id = :id";
            $stmt = self::$db->prepare($query);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->closeCursor();
            return $this->redirectToRoute('consult.index');
        } catch (\Throwable $th) {
            throw $th;
            error_log($th);
        }
    }

    /**
     * @Route("/{id}", name="show")
     */
    public function show($id)
    {
        return $this->render('consult/show.html.twig', [
            'title' => 'Consultorio',
            'consult' => self::getConsults($id)[0],
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     */
    public function edit($id, Request $request): Response
    {
        $consult = self::getConsults($id)[0];
        $recordatorio = new \DateTime($consult['recordatorio']);
        $consult['recordatorio'] = $recordatorio;

        $duracion = new \DateTime($consult['duracion']);
        $consult['duracion'] = $duracion;

        dump(['co' => $consult]);
        $form = $this->handleForm($request, $consult);
        if ($form->isSubmitted()) {
            return $this->redirectToRoute('consult.index');
        }
        return $this->render('consult/form.html.twig', [
            'title' => 'Consultorio',
            'consult' => self::getConsults($id)[0],
            'form' => $form->createView(),
        ]);
    }
}
