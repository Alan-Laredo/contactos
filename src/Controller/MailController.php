<?php

# php -S 127.0.0.1:8000 -t public
# tienes que estar ubicado en la carpeta raiz del server

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Entity\Mail;
use App\Form\MailType;

/**
 * @Route("/mail", name="mail.")
 */
class MailController extends AbstractController
{
    private $sidebar = array(
        [
            'name' => 'General',
            'path' => "mail.index",
            'params' => ['folder' => 'INBOX'],
            'elements' => [],
        ]
    );

    private $folders = [];

    // imap server connection
    public $conn;

    // inbox storage and inbox message count
    private $inbox;
    private $msg_cnt;

    // Outbox storage and current message
    private $outbox;
    private $msg;

    // Email credentials
    private $inServer  = '{a2plcpnl0726.prod.iad2.secureserver.net}';
    private $outServer = 'a2plcpnl0726.prod.iad2.secureserver.net';
    private $user      = 'dev@teemmx.com';
    private $pass      = 'ylc$ZmO.rs@1';
    private $inPort    = 993; // adjust according to server settings
    private $outPort   = 465; // adjust according to server settings

    // connect to the server and get the inbox emails
    function __construct()
    {
        $this->connect();
        $this->folders();
        $this->msg = new Mail();
    }

    // close the server connection
    private function close()
    {
        $this->inbox = array();
        $this->msg_cnt = 0;

        imap_close($this->conn);
    }

    // open the server connection
    private function connect()
    {
        $this->conn = imap_open($this->inServer, $this->user, $this->pass);

        //Server settings
        $this->outbox = new PHPMailer(true);                          // Passing `true` enables exceptions
        $this->outbox->SMTPDebug = 2;                                 // Enable verbose debug output
        $this->outbox->isSMTP();                                      // Set mailer to use SMTP
        $this->outbox->Username = $this->user;                        // SMTP username
        $this->outbox->Password = $this->pass;                        // SMTP password

        $this->outbox->Host = $this->outServer;
        $this->outbox->SMTPAuth = true;                               // Enable SMTP authentication
        $this->outbox->SMTPSecure = false;                            // Enable TLS encryption, `ssl` also accepted
        $this->outbox->Port = $this->outPort;                         // TCP port to connect to
    }

    // reopen the server connection
    private function folders($server = false)
    {
        $list = imap_list($this->conn, $this->inServer, "*");

        //make sure the list is an array
        if (is_array($list)) {

            //loop through rach array index
            foreach ($list as $val) {

                //remove  any } charactors from the folder
                if (preg_match("/}/i", $val)) {
                    $arr = explode('}', $val);
                }

                //also remove the ] if it exists, normally Gmail have them
                if (preg_match("/]/i", $val)) {
                    $arr = explode(']/', $val);
                }

                //remove any slashes
                $folder = trim(stripslashes($arr[1]));

                //remove inbox. from the folderName its not needed for displaying purposes
                $folderName = str_replace('INBOX.', '', $folder);

                array_push(
                    $this->folders,
                    [
                        'name' => $folderName,
                        'path' => 'mail.index',
                        'params' => ['folder' => $folder],
                        'parsed' => ucwords(strtolower(imap_utf7_decode($folderName))),
                    ]
                );
            }
        } else {
            array_push($this->folders, "Folders not currently availablen");
        }
        $this->sidebar[0]['elements'] = $this->folders;
    }

    // move the message to a new folder
    public function move($msg_index, $folder = 'INBOX.Processed')
    {
        // move on server
        imap_mail_move($this->conn, $msg_index, $folder);
        imap_expunge($this->conn);

        // re-read the inbox
        $this->inbox();
    }

    // get a specific message (1 = first email, 2 = second email, etc.)
    public function get($msg_index = NULL)
    {
        $msg_index = $msg_index - 1;
        if (count($this->inbox) <= 0) {
            return array();
        } elseif (!is_null($msg_index) && isset($this->inbox[$msg_index])) {
            return $this->inbox[$msg_index];
        }

        return $this->inbox[0];
    }

    // get a specific message (1 = first email, 2 = second email, etc.)
    public function send($msg_index = NULL)
    {
        $msg_index = $msg_index - 1;
        if (count($this->inbox) <= 0) {
            return array();
        } elseif (!is_null($msg_index) && isset($this->inbox[$msg_index])) {
            return $this->inbox[$msg_index];
        }

        return $this->inbox[0];
    }


    public function setField($set, $get)
    {
        if ($this->msg->{'get' . $get}() !== null) {
            foreach (explode(';', $this->msg->{'get' . $get}()) as $recipient) {
                $recipient = explode(',', $recipient);
                $recipient['mail'] = str_replace(' ', '', $recipient[0]);
                unset($recipient[0]);
                $recipient['name'] = array_key_exists(1, $recipient) ? ltrim($recipient[1]) : null;
                unset($recipient[1]);

                if ($recipient !== []) {
                    if ($recipient['name'] === null) {
                        $this->outbox->{'add' . $set}($recipient['mail']);
                    } else {
                        $this->outbox->{'add' . $set}($recipient['mail'], $recipient['name']);
                    }
                }
            }
        }
    }

    // read the inbox
    private function inbox()
    {
        $this->msg_cnt = imap_num_msg($this->conn);

        $in = array();
        for ($i = 1; $i <= $this->msg_cnt; $i++) {
            $in[] = array(
                'index'     => $i,
                'header'    => imap_headerinfo($this->conn, $i),
                'body'      => imap_body($this->conn, $i),
                'fetchbody' => str_replace("\r\n", "<br>",  imap_fetchbody($this->conn, $i, 1)),
                'structure' => imap_fetchstructure($this->conn, $i)
            );
        }

        $this->inbox = $in;
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        try {

            $this->outbox->setFrom($this->user, 'Client');
        } catch (Exception $e) {
            error_log($e);
            error_log('Message could not be sent.');
            error_log('Mailer Error: ' . $this->outbox->ErrorInfo);
        }
        $form = $this->createForm(MailType::class, $this->msg);
        $form->handleRequest($request);
        // dump($form);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->msg = $form->getData();
            try {
                //Recipients
                $this->outbox->setFrom($this->user, 'Client');
                $this->setField('Address', 'Receiver');
                $this->setField('CC', 'Cc');
                $this->setField('BCC', 'Cco');
                //$this->outbox->addAddress('address@example.com', 'Address');  // Add a recipient, Name is optional
                //$this->outbox->addReplyTo('info@example.com', 'Information');
                //$this->outbox->addCC('cc@example.com');
                //$this->outbox->addBCC('bcc@example.com');

                //Attachments
                //$this->outbox->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                //$this->outbox->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                //Content
                $this->outbox->isHTML(true);                                  // Set email format to HTML
                $this->outbox->Subject = $this->msg->getSubject();
                $this->outbox->Body    = $this->msg->getBody();
                //$this->outbox->AltBody = 'This is the body in plain text for non-HTML mail clients';

                dump([
                    'from' => $this->outbox->From,
                    'to' => $this->outbox->getToAddresses(),
                    'cc' => $this->outbox->getCcAddresses(),
                    'bcc' => $this->outbox->getBccAddresses(),
                    'ReplyTo' => $this->outbox->getReplyToAddresses(),
                    'all_recipients' => $this->outbox->getAllRecipientAddresses(),
                    'attachment' => $this->outbox->getAttachments(),
                    'subject' => $this->outbox->Subject,
                    'body' => $this->outbox->Body,
                ]);
                dump($this->outbox);
                $this->outbox->send();
                error_log('Message has been sent');
                return $this->redirectToRoute('mail.index', ['folder' => 'INBOX']);
            } catch (Exception $e) {
                error_log($e);
                error_log('Message could not be sent.');
                error_log('Mailer Error: ' . $this->outbox->ErrorInfo);
            }

            // return $this->redirectToRoute('mail.index', ['folder' => 'INBOX']);
        }

        return $this->render('mail/new.html.twig', [
            'mail' => $this->outbox,
            'form' => $form->createView(),
            'title' => 'Nuevo Correo',
            'mails' => $this->inbox,
            'sidebar' => $this->sidebar,
        ]);
    }

    /**
     * @Route("/{folder}", name="index")
     */
    public function index($folder)
    {
        imap_reopen($this->conn, $this->inServer . $folder);
        $this->inbox();

        return $this->render('mail/index.html.twig', [
            'title' => $folder,
            'mails' => $this->inbox,
            'sidebar' => $this->sidebar,
            'folder' => $folder,
        ]);
    }

    /**
     * @Route("/{folder}/{mail}", name="show")
     */
    public function show($folder, $mail)
    {
        imap_reopen($this->conn, $this->inServer . $folder);
        $this->inbox();
        return $this->render('mail/show.html.twig', [
            'mail' => $this->get($mail),
            'title' => $folder,
            'mails' => $this->inbox,
            'sidebar' => $this->sidebar,
        ]);
    }
}
