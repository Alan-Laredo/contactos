<?php

namespace App\Controller;

use App\Entity\Todo;
use App\Form\TodoType;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Query;

/**
 * @Route("/calendar", name="calendar.")
 */
class CalendarController extends AbstractController
{
    private $sidebar = array(
        [
            'name' => 'General',
            'path' => 'calendar.index',
            'elements' => [
                [
                    'name' => 'ToDo', 'path' => 'calendar.index',
                    'params' => ['calendar' => 'ToDo'],
                ],
                [
                    'name' => 'Citas', 'path' => 'calendar.index',
                    'params' => ['calendar' => 'Citas'],
                ],
                [
                    'name' => 'Fechas no laborables', 'path' => 'calendar.index',
                    'params' => ['calendar' => 'NL'],
                ],
            ],
        ]
    );

    private $todoRepository;
    private $events = [];

    public function __construct(TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;
        $this->events();
    }

    public function events($calendar = null)
    {
        $events = [];
        
        if ($calendar === null) {
            $events_obj = $this->todoRepository->findAll();
        } else {
            $calendar = strtolower($calendar);
            $events_obj = $this->todoRepository->findBy(['calendario' => $calendar]);
        }
        
        foreach ($events_obj as $todo) {
            array_push($events, $todo->getAll());
        }

        $this->events = $events;
    }

    /**
     * @Route("/")
     * @Route("/{calendar}", name="index", methods={"GET"})
     */
    public function index($calendar = null): Response
    {
        $this->events($calendar);
        return $this->render('calendar/index.html.twig', [
            'title' => 'General',
            'events' => $this->events,
            'sidebar' => $this->sidebar,
        ]);
    }
}
