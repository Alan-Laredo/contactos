<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\DBAL\DriverManager;

class DataController extends AbstractController
{
    private static $conn;
    
    public static function connect()
    {
        self::$conn = DriverManager::getConnection([
            'driver'    => "pdo_mysql",
            'host'      => "localhost",
            'dbname'    => "project1",
            'user'      => "root",
            'password'  => "",
            'charset'   => 'utf8'
        ]);
        return self::$conn;
    }
}
