<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

/**
 * @Route("/expense", name="expense.")
 */
class ExpenseController extends AbstractController
{
    private $sidebar = array(
        [
            'name' => 'General',
            'path' => 'expense.index',
            'elements' => [],
        ]
    );

    // db connection
    private static $db;
    private static $initialized = false;

    public function __construct()
    {
        self::initialize();
        foreach (self::getExpenseCategories() as $category) {
            $this->sidebar[0]['elements'][] =
                [
                    'name' => $category['name'],
                    'path' => 'expense.index',
                    'params' => [
                        'category' => $category['name']
                    ],
                ];
        }
    }

    private static function initialize()
    {
        if (self::$initialized)
            return;

        self::$db = DataController::connect();
    }

    public static function getExpenseCategories($id = null, $name = null)
    {
        self::initialize();
        $query = 'SELECT
                *
            FROM
                expense_category c 
            WHERE c.active = 1';
        if (isset($id)) {
            $query .= ' AND c.id = :id ';
        }
        if (isset($name)) {
            $query .= ' AND c.name = :name ';
        }
        $stmt = self::$db->prepare($query);
        if (isset($id)) {
            $stmt->bindValue(':id', $id);
        }
        if (isset($name)) {
            $stmt->bindValue(':name', $name);
        }
        $stmt->execute();
        $results = $stmt->fetchAll();
        $stmt->closeCursor();
        dump(['categories' => $results]);
        return $results;
    }

    public static function getExpenses($id = null, $category = null)
    {
        self::initialize();
        $query = 'SELECT
                e.id,
                e.fecha,
                e.descripcion,
                e.cantidad,
                e.monto,
                e.categoria,
                c.name AS nombreCategoria,
                c.type
            FROM
                expense e 
                LEFT JOIN expense_category c ON
                    e.categoria = c.id
            WHERE e.active = 1';
        if (isset($id)) {
            $query .= ' AND e.id = :id ';
        }
        if (isset($category)) {
            $query .= ' AND c.name = :category ';
        }
        $stmt = self::$db->prepare($query);
        if (isset($id)) {
            $stmt->bindValue(':id', $id);
        }
        if (isset($category)) {
            $stmt->bindValue(':category', $category);
        }
        $stmt->execute();
        $results = $stmt->fetchAll();
        foreach ($results as $key => $result) {
            $results[$key]['type'] = ucfirst($result['type']);
        }
        $stmt->closeCursor();
        dump(['expenses' => $results]);
        return $results;
    }

    private function expenseForm(Request $request, $data = null)
    {

        $categories = self::getExpenseCategories();
        foreach ($categories as $category) {
            $choices[$category['name']] = $category['id'];
        }
        if (isset($data['fecha'])) {
            $data['fecha'] = new \DateTime($data['fecha']);
        } else {
            $data['fecha'] = new \DateTime();
        }
        $form = $this->createFormBuilder($data)
            ->add('descripcion', TextareaType::class)
            ->add('cantidad', NumberType::class, [
                'html5' => true,
            ])
            ->add('monto', NumberType::class, [
                'html5' => true,
            ])
            ->add('categoria', ChoiceType::class, [
                'placeholder' => 'Categoria',
                'choices' => $choices,
            ])
            ->add('fecha', DateTimeType::class, [
                // 'placeholder' => 'Categoria',
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitedData = $form->getData();
            dump(['submitedData' => $submitedData, 'data' => $data]);
            try {
                $query = '';
                if (isset($data['id'])) {
                    $query = "UPDATE expense
                        SET descripcion = :descripcion, cantidad = :cantidad, categoria = :categoria, monto = :monto, fecha = :fecha
                        WHERE id = :id";
                } else {
                    $query = "INSERT 
                        INTO expense (descripcion, cantidad, categoria, monto, fecha) 
                        VALUES (:descripcion, :cantidad, :categoria, :monto, :fecha)";
                }
                $stmt = self::$db->prepare($query);
                if (isset($data['id'])) {
                    $stmt->bindValue(':id', $data['id']);
                }
                $stmt->bindValue(':descripcion', $submitedData['descripcion']);
                $stmt->bindValue(':cantidad', $submitedData['cantidad']);
                $stmt->bindValue(':categoria', $submitedData['categoria']);
                $stmt->bindValue(':monto', $submitedData['monto']);
                $stmt->bindValue(':fecha', $submitedData['fecha']->format('Y-m-d H:i:s'));
                $stmt->execute();
                $stmt->closeCursor();
            } catch (\Throwable $th) {
                throw $th;
                error_log($th);
            }
        }

        return $form;
    }

    private function categoryForm(Request $request, $data = null)
    {
        $form = $this->createFormBuilder($data)
            ->add('name', TextType::class)
            ->add('type', ChoiceType::class, [
                'placeholder' => 'Categoria',
                'choices' => ['gasto' => 'gasto', 'ingreso' => 'ingreso'],
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitedData = $form->getData();
            dump(['submitedData' => $submitedData, 'data' => $data]);
            try {
                $query = '';
                if ($data) {
                    $query = "UPDATE expense_category
                        SET name = :name, type = :type
                        WHERE id = :id";
                } else {
                    $query = "INSERT 
                        INTO expense_category (name, type) 
                        VALUES (:name, :type)";
                }
                $stmt = self::$db->prepare($query);
                if ($data) {
                    $stmt->bindValue(':id', $data['id']);
                }
                $stmt->bindValue(':name', $submitedData['name']);
                $stmt->bindValue(':type', $submitedData['type']);
                $stmt->execute();
                $stmt->closeCursor();
            } catch (\Throwable $th) {
                throw $th;
                error_log($th);
            }
        }

        return $form;
    }

    /**
     * @Route("/newCategory", name="newCategory")
     */
    public function newCategory(Request $request): Response
    {
        $form = $this->categoryForm($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('expense.index');
        }

        return $this->render('expense/form_category.html.twig', [
            'title' => 'Nueva Categoria',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{category}/new")
     * @Route("/new", name="newExpense")
     */
    public function newExpense($category = null, Request $request): Response
    {
        $categories = self::getExpenseCategories(null, $category);
        $category = count($categories) === 1 ? $categories[0] : null;
        $form = $this->expenseForm(
            $request,
            ['categoria' => $category['id']]
        );
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('expense.index');
        }

        return $this->render('expense/form_expense.html.twig', [
            'title' => 'Nuevo Gasto',
            'category' => $category['name'],
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{category}/{id}", name="delete", methods={"DELETE"})
     */
    public function delete($category, $id, Request $request): Response
    {
        try {
            $query = "UPDATE expense
                SET active = 0
                WHERE id = :id";
            $stmt = self::$db->prepare($query);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->closeCursor();
            return $this->redirectToRoute('expense.index');
        } catch (\Throwable $th) {
            throw $th;
            error_log($th);
        }
    }

    /**
     * @Route("/{category}/{id}", name="show")
     */
    public function show($category, $id)
    {
        $expense = self::getExpenses($id)[0];
        dump($expense);
        return $this->render('expense/show.html.twig', [
            'title' => $category . ' ' . $expense['id'],
            'expense' => $expense,
            'sidebar' => $this->sidebar,
        ]);
    }

    /**
     * @Route("/{category}/{id}/edit", name="editExpense")
     */
    public function editExpense($category, $id, Request $request): Response
    {
        $expense = self::getExpenses($id)[0];
        $form = $this->expenseForm($request, $expense);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('expense.index');
        }

        return $this->render('expense/form_expense.html.twig', [
            'title' => $category . ' ' . $expense['id'],
            'expense' => $expense,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/", name="index")
     * @Route("/{category}", name="index")
     */
    public function index($category = null)
    {
        $expenses = self::getExpenses(null, $category);
        $total = 0;
        foreach ($expenses as $expense) {
            if ($expense['type'] === 'Gasto') {
                $total -= $expense['cantidad'] * $expense['monto'];
            }
            if ($expense['type'] === 'Ingreso') {
                $total += $expense['cantidad'] * $expense['monto'];
            }
        }
        return $this->render('expense/index.html.twig', [
            'title' => $category ? $category : 'Control de Gastos',
            'category' => $category,
            'expenses' => $expenses,
            'total' => $total,
            'sidebar' => $this->sidebar,
        ]);
    }
}
