<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodoRepository")
 */
class Todo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $recordatorio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $calendario;

    /**
     * @ORM\Column(type="boolean")
     */
    private $done;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getRecordatorio(): ?\DateTimeInterface
    {
        return $this->recordatorio;
    }

    public function setRecordatorio(\DateTimeInterface $recordatorio): self
    {
        $this->recordatorio = $recordatorio;

        return $this;
    }

    public function getCalendario(): ?string
    {
        return $this->calendario;
    }

    public function setCalendario(string $calendario): self
    {
        $this->calendario = $calendario;

        return $this;
    }

    public function getDone(): ?bool
    {
        return $this->done;
    }

    public function setDone(bool $done): self
    {
        $this->done = $done;

        return $this;
    }
	public function getAll(){
		return get_object_vars($this);
	}
}
